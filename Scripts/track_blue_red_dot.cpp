// g++ -IC:/OpenCV/build/opencv-4.0.1/install/include main.cpp -o test.exe -LC:/OpenCV/build/opencv-4.0.1/install/x64/mingw/lib -lopencv_core401 -lopencv_imgcodecs401 -lopencv_imgproc401 -lopencv_videoio401 -lopencv_highgui401

#include <stdio.h>
#include <algorithm>
#include <opencv2/video.hpp>
#include <opencv2/imgproc.hpp> 
#include <opencv2/highgui.hpp>

using std::min;
using std::max;
using namespace cv;

bool change_orientation = true;

const Mat rect5x5 = getStructuringElement(MORPH_RECT, Size(5, 5));

Mat g_display, g_mask;
bool g_lbdown = false;
int g_radius = 20;
void mouse_draw_mask(int event, int x, int y, int flags, void *param);
bool set_static_mask(VideoCapture &cap);

Mat g_hsv;
void mouse_get_hsv(int event, int x, int y, int flags, void *param);
bool inRange_hsv(const Mat &src, const Vec3b &lb, const Vec3b &ub, Mat &mask);
bool get_center_of_nonzero(const Mat &src, Point2f &center);
bool set_hsv_range(VideoCapture &cap, Vec3b &lb_index, Vec3b &ub_index, Vec3b &lb_thumb, Vec3b &ub_thumb);

int main(int argc, char *argv[]){   // test.exe video_name
    Mat frame, hsv, mask_index, mask_thumb;
    VideoCapture cap;
    float dt;
    Vec3b lb_index, ub_index, lb_thumb, ub_thumb;
    Point2f center_index, center_thumb;
    char filename[256];
    FILE *fp;
    int i, iframe;

    Mat labels, stats, centroids;
    int n_cc, max_area;

    if(argc != 2){
        fprintf(stderr, "Wrong input arguments.\n");
        return 1;
    }

    cap.open(argv[1]);
    if(!cap.isOpened()){
        fprintf(stderr, "Cannot open the file\n");
        return 1;
    }
    dt = 1000. / cap.get(CAP_PROP_FPS);

    if(!set_static_mask(cap)){
        fprintf(stderr, "Didn't setup static mask correctly\n");
        return 1;
    }
    imshow("Static mask. Press any key to continue.", g_mask);
    waitKey(0);
    destroyAllWindows();

    if(set_hsv_range(cap, lb_index, ub_index, lb_thumb, ub_thumb)){
        sprintf(filename, "%s.csv", argv[1]);
        fp = fopen(filename, "w");
        fprintf(fp, "t (ms), x_thumb, y_thumb, x_index, y_index\n");
        // start record
        iframe = 0;
        cap.set(CAP_PROP_POS_FRAMES, 0);
        while(cap.read(frame)){
            if(frame.empty())
                break;
            frame &= g_mask;
            cvtColor(frame, hsv, COLOR_BGR2HSV);
            // index
            inRange_hsv(hsv, lb_index, ub_index, mask_index);
            morphologyEx(mask_index, mask_index, MORPH_OPEN, rect5x5);
            // get_center_of_nonzero(mask_index, center_index);
            n_cc = connectedComponentsWithStats(mask_index, labels, stats, centroids);
            center_index = Point(0, 0);
            max_area = 0;
            for(i = 1; i < n_cc; ++i)
                if(stats.at<int>(i, 4) > max_area){
                    center_index = Point(centroids.at<double>(i, 0), centroids.at<double>(i, 1));
                    max_area = stats.at<int>(i, 4);
                }
            // thumb
            inRange_hsv(hsv, lb_thumb, ub_thumb, mask_thumb);
            morphologyEx(mask_thumb, mask_thumb, MORPH_OPEN, rect5x5); // change to other size
            // get_center_of_nonzero(mask_thumb, center_thumb);
            n_cc = connectedComponentsWithStats(mask_thumb, labels, stats, centroids);
            center_thumb = Point(0, 0);
            max_area = 0;
            for(i = 1; i < n_cc; ++i)
                if(stats.at<int>(i, 4) > max_area){
                    center_thumb = Point(centroids.at<double>(i, 0), centroids.at<double>(i, 1));
                    max_area = stats.at<int>(i, 4);
                }

            // output
            if(change_orientation)
                fprintf(fp, "%f, %f, %f, %f, %f\n", (iframe++) * dt, center_thumb.y, center_thumb.x, center_index.y, center_index.x);
            else
                fprintf(fp, "%f, %f, %f, %f, %f\n", (iframe++) * dt, center_thumb.x, center_thumb.y, center_index.x, center_index.y);
        }
        fclose(fp);
    }
    else
        fprintf(stderr, "Cannot set the hsv filter correctly\n");

    cap.release();
    return 0;
}

void mouse_draw_mask(int event, int x, int y, int flags, void *param){
    if(event == EVENT_MOUSEMOVE){
        if(g_lbdown){
            circle(g_display, Point(x, y), g_radius, Scalar(0, 0, 0), FILLED);
            circle(g_mask, Point(x, y), g_radius, Scalar(0, 0, 0), FILLED);
        }
    }
    else if(event == EVENT_LBUTTONDOWN){
        circle(g_display, Point(x, y), g_radius, Scalar(0, 0, 0), FILLED);
        circle(g_mask, Point(x, y), g_radius, Scalar(0, 0, 0), FILLED);
        g_lbdown = true;
    }
    else if(event == EVENT_LBUTTONUP){
        circle(g_display, Point(x, y), g_radius, Scalar(0, 0, 0), FILLED);
        circle(g_mask, Point(x, y), g_radius, Scalar(0, 0, 0), FILLED);
        g_lbdown = false;
    }
}
bool set_static_mask(VideoCapture &cap){
    Mat frame;
    const String wnd_draw = "Draw a mask";
    double current_frame = cap.get(CAP_PROP_POS_FRAMES);
    int key = -1;

    namedWindow(wnd_draw);
    setMouseCallback(wnd_draw, mouse_draw_mask);
    cap.read(frame);
    frame.copyTo(g_display);
    g_mask.create(frame.size(), frame.type());
    g_mask.setTo(Scalar(255, 255, 255));
    while(true){
        if(key == 13 || key == 27)
            break;
        else if(key == 32){  // reset
            frame.copyTo(g_display);
            g_mask.setTo(Scalar(255, 255, 255));
        }
        imshow(wnd_draw, g_display);
        key = waitKey(1);
    }

    destroyWindow(wnd_draw);
    cap.set(CAP_PROP_POS_FRAMES, current_frame);

    return key == 27? false : true;
}

void mouse_get_hsv(int event, int x, int y, int flags, void *param){
    if(event == EVENT_LBUTTONUP){
        const Vec3b &p = g_hsv.at<Vec3b>(y, x);
        printf("h: %d, s: %d, v: %d\n", p[0], p[1], p[2]);
    }
}
bool inRange_hsv(const Mat &src, const Vec3b &lb, const Vec3b &ub, Mat &mask){
    if(lb[0] <= ub[0])
        inRange(src, lb, ub, mask);
    else{
        Mat tmp;
        inRange(src, Vec3b(0, lb[1], lb[2]), ub, mask);
        inRange(src, lb, Vec3b(180, ub[1], ub[2]), tmp);
        mask |= tmp;
    }
    return true;
}
bool get_center_of_nonzero(const Mat &src, Point2f &center){
    int cnt = 0;
    const uchar *p;

    if(src.type() != CV_8U){
        fprintf(stderr, "Type of src in get_center_of_nonzero() should be CV_8U\n");
        return false;
    }

    // calculate
    center = Point2f(0, 0);
    for(int i = 0; i < src.rows; ++i){
        p = src.ptr<uchar>(i);
        for(int j = 0; j < src.cols; ++j)
            if(p[j] != 0){
                center += Point2f(j, i);
                ++cnt;
            }
    }
    if(cnt != 0){
        center /= cnt;
        return true;
    }
    else{
        fprintf(stderr, "Could not find the center in get_center_of_nonzero()\n");
        return false;
    }
}
bool set_hsv_range(VideoCapture &cap, Vec3b &lb_index, Vec3b &ub_index, Vec3b &lb_thumb, Vec3b &ub_thumb){
    Mat frame, mask_index, mask_thumb, mask;
    Point2f center_index, center_thumb;
    float fps, dt, nframes;
    bool pause = true;
    int key = -1;
    double current_frame = cap.get(CAP_PROP_POS_FRAMES);
    int i;

    Mat labels, stats, centroids;
    int n_cc, max_area;

    const String wnd_display = "Click to show hsv value, press enter to continue";
    const String wnd_panel = "Panel";
    const String wnd_mask = "Masked result";

    int h_lb_index = 90, h_ub_index = 115, s_lb_index = 80, s_ub_index = 255, v_lb_index = 100, v_ub_index = 255;
    int h_lb_thumb = 165, h_ub_thumb = 5, s_lb_thumb = 165, s_ub_thumb = 255, v_lb_thumb = 120, v_ub_thumb = 255;

    namedWindow(wnd_display);
    namedWindow(wnd_panel);
    namedWindow(wnd_mask);

    setMouseCallback(wnd_display, mouse_get_hsv);
    resizeWindow(wnd_panel, Size(600, 800));
    createTrackbar("h_lb_index", wnd_panel, &h_lb_index, 180);
    createTrackbar("h_ub_index", wnd_panel, &h_ub_index, 180);
    createTrackbar("s_lb_index", wnd_panel, &s_lb_index, 255);
    createTrackbar("s_ub_index", wnd_panel, &s_ub_index, 255);
    createTrackbar("v_lb_index", wnd_panel, &v_lb_index, 255);
    createTrackbar("v_ub_index", wnd_panel, &v_ub_index, 255);
    createTrackbar("h_lb_thumb", wnd_panel, &h_lb_thumb, 180);
    createTrackbar("h_ub_thumb", wnd_panel, &h_ub_thumb, 180);
    createTrackbar("s_lb_thumb", wnd_panel, &s_lb_thumb, 255);
    createTrackbar("s_ub_thumb", wnd_panel, &s_ub_thumb, 255);
    createTrackbar("v_lb_thumb", wnd_panel, &v_lb_thumb, 255);
    createTrackbar("v_ub_thumb", wnd_panel, &v_ub_thumb, 255);

    fps = cap.get(CAP_PROP_FPS);
    dt = 1000. / fps; // unit: ms
    nframes = cap.get(CAP_PROP_FRAME_COUNT);

    cap.set(CAP_PROP_POS_FRAMES, 0);
    cap.read(frame);
    if(frame.empty())
        return false;
    frame &= g_mask;    
    cvtColor(frame, g_hsv, COLOR_BGR2HSV);
    imshow(wnd_display, frame);
    while(true){
        if(!pause || key == 44 || key == 46){
            cap.read(frame);    // update if not puase, or press '<' or '>'
            if(frame.empty()){  // restart
                cap.set(CAP_PROP_POS_FRAMES, 0);
                cap.read(frame);
                if(frame.empty())
                    break;
            }
            frame &= g_mask;
            cvtColor(frame, g_hsv, COLOR_BGR2HSV);
        }
        // g_hsv filter for index finger
        inRange_hsv(g_hsv, Vec3b(h_lb_index, s_lb_index, v_lb_index), Vec3b(h_ub_index, s_ub_index, v_ub_index), mask_index);
        morphologyEx(mask_index, mask_index, MORPH_OPEN, rect5x5);
        // get_center_of_nonzero(mask_index, center_index);
        n_cc = connectedComponentsWithStats(mask_index, labels, stats, centroids);
        center_index = Point(0, 0);
        max_area = 0;
        for(i = 1; i < n_cc; ++i)
            if(stats.at<int>(i, 4) > max_area){
                center_index = Point(centroids.at<double>(i, 0), centroids.at<double>(i, 1));
                max_area = stats.at<int>(i, 4);
            }
        // g_hsv filter for thumb
        inRange_hsv(g_hsv, Vec3b(h_lb_thumb, s_lb_thumb, v_lb_thumb), Vec3b(h_ub_thumb, s_ub_thumb, v_ub_thumb), mask_thumb);
        morphologyEx(mask_thumb, mask_thumb, MORPH_OPEN, rect5x5);
        // get_center_of_nonzero(mask_thumb, center_thumb);
        n_cc = connectedComponentsWithStats(mask_thumb, labels, stats, centroids);
        center_thumb = Point(0, 0);
        max_area = 0;
        for(i = 1; i < n_cc; ++i)
            if(stats.at<int>(i, 4) > max_area){
                center_thumb = Point(centroids.at<double>(i, 0), centroids.at<double>(i, 1));
                max_area = stats.at<int>(i, 4);
            }

        // display
        mask = mask_index | mask_thumb;
        circle(mask, center_index, 2, Scalar(0, 0, 0), 2);
        circle(mask, center_thumb, 2, Scalar(0, 0, 0), 2);
        imshow(wnd_display, frame);
        imshow(wnd_mask, mask);
        key = waitKey(1);

        // keybd event
        if(key == 27 || key == 13) // ESC, RETURN
            break;
        else if(key == 32) // SPACE
            pause = !pause;
        else if(key == 44) // '<'
            cap.set(CAP_PROP_POS_FRAMES, max(cap.get(CAP_PROP_POS_FRAMES) - fps, 0.));
        else if(key == 46) // '>'
            cap.set(CAP_PROP_POS_FRAMES, min(cap.get(CAP_PROP_POS_FRAMES) + fps, nframes - 1.));
    }
    destroyAllWindows();
    cap.set(CAP_PROP_POS_FRAMES, current_frame);

    if(key == 13){
        lb_index = Vec3b(h_lb_index, s_lb_index, v_lb_index);
        ub_index = Vec3b(h_ub_index, s_ub_index, v_ub_index);
        lb_thumb = Vec3b(h_lb_thumb, s_lb_thumb, v_lb_thumb);
        ub_thumb = Vec3b(h_ub_thumb, s_ub_thumb, v_ub_thumb);
        return true;
    }
    else
        return false;
}