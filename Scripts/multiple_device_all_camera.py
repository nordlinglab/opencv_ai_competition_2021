import cv2
import depthai as dai
import contextlib


fps=[60,60,60]
def createPipeline(fps):
    pipeline = dai.Pipeline()
    cam_rgb = pipeline.createColorCamera()
    cam_rgb.setResolution(dai.ColorCameraProperties.SensorResolution.THE_1080_P)
    cam_rgb.setFps(fps[0])
    print('Created color camera object')

    monoLeft = pipeline.createMonoCamera()
    monoRight = pipeline.createMonoCamera()
    monoLeft.setBoardSocket(dai.CameraBoardSocket.LEFT)
    monoRight.setBoardSocket(dai.CameraBoardSocket.RIGHT)
    monoLeft.setResolution(dai.MonoCameraProperties.SensorResolution.THE_720_P)
    monoRight.setResolution(dai.MonoCameraProperties.SensorResolution.THE_720_P)
    monoLeft.setFps(fps[1])
    monoRight.setFps(fps[2])
    print('Created mono camera objects')

    ve_rgb = pipeline.createVideoEncoder()
    ve_rgb.setDefaultProfilePreset(1920, 1080, fps[0], dai.VideoEncoderProperties.Profile.H265_MAIN)
    xout_ve_rgb = pipeline.createXLinkOut()
    xout_ve_rgb.setStreamName('rgb')

    ve_left = pipeline.createVideoEncoder()
    ve_left.setDefaultProfilePreset(1280, 720, fps[1], dai.VideoEncoderProperties.Profile.H264_MAIN)
    xout_ve_left = pipeline.createXLinkOut()
    xout_ve_left.setStreamName('left')

    ve_right = pipeline.createVideoEncoder()
    ve_right.setDefaultProfilePreset(1280, 720, fps[2], dai.VideoEncoderProperties.Profile.H264_MAIN)
    xout_ve_right = pipeline.createXLinkOut()
    xout_ve_right.setStreamName('right')
    print('Created video encoder objects')
    # Linking
    cam_rgb.video.link(ve_rgb.input)
    monoLeft.out.link(ve_left.input)
    monoRight.out.link(ve_right.input)

    ve_rgb.bitstream.link(xout_ve_rgb.input)
    ve_left.bitstream.link(xout_ve_left.input)
    ve_right.bitstream.link(xout_ve_right.input)

    print('Created pipeline')
    return pipeline


pipeline = createPipeline(fps)
q_rgb_list = []
q_left_list = []
q_right_list = []
with contextlib.ExitStack() as stack:
    for device_info in dai.Device.getAllAvailableDevices():
        device = stack.enter_context(dai.Device(pipeline, device_info))

        device.startPipeline()

        q_rgb = device.getOutputQueue(name='rgb', maxSize=fps[0], blocking=True)
        q_left = device.getOutputQueue(name='left', maxSize=fps[1], blocking=True)
        q_right = device.getOutputQueue(name='right', maxSize=fps[2], blocking=True)
        q_rgb_list.append(q_rgb)
        q_left_list.append(q_left)
        q_right_list.append(q_right)

    path = 'data/'
    rgb_path = path + 'rgb_1.h265'
    left_path = path + 'left_1.h264'
    right_path = path + 'right_1.h264'

    path = 'data/'
    rgb_path_2 = path + 'rgb_2.h265'
    left_path_2 = path + 'left_2.h264'
    right_path_2 = path + 'right_2.h264'
    with open(left_path, 'wb') as fileMonoLeft, open(rgb_path, 'wb') as fileRGB, open(right_path,'wb') as fileMonoRight,open(left_path_2, 'wb') as fileMonoLeft_2, open(rgb_path_2, 'wb') as fileRGB_2, open(right_path_2,'wb') as fileMonoRight_2:
        while True:
            try:
                # Empty each queue
                while q_rgb_list[0].has():
                    in_rgb = q_rgb_list[0].get()
                    in_rgb.getData().tofile(fileRGB)

                while q_rgb_list[1].has():
                    in_rgb_2 = q_rgb_list[1].get()
                    in_rgb_2.getData().tofile(fileRGB_2)

                while q_left_list[0].has():
                    in_left = q_left_list[0].get()
                    in_left.getData().tofile(fileMonoLeft)

                while q_left_list[1].has():
                    in_left_2 = q_left_list[1].get()
                    in_left_2.getData().tofile(fileMonoLeft_2)

                while q_right_list[0].has():
                    in_right = q_right_list[0].get()
                    in_right.getData().tofile(fileMonoRight)

                while q_right_list[1].has():
                    in_right_2 = q_right_list[1].get()
                    in_right_2.getData().tofile(fileMonoRight_2)
            except KeyboardInterrupt:
                break
