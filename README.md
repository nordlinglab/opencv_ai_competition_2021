# README #

This repository contain the codes and results of our work for opencv AI competition 2021

### Why/What/How?  ###

With the three cameras, we can determine the position of each joint of interest in 3D to calculate the distance between the index finger and thumb in finger tapping and distance between the index finger and palm in hand movement. With this as ground truth, we aim to train a 3D CNN to predict these metrics based on the data from the OAK-D. We use these trajectories to detect hesitations, pauses, changes in amplitude and frequency that are used to provide an UPDRS score. An overview of this proposal is provided in Figure.

![picture](Figures/WWH_QuantPD_edit.png)

As shown in Figure, in the early days, doctors were used to diagnose the degree of Parkinson's disease, but this was too wasteful of resources and time. 
Our approach consists of three main steps:
* collect the patient's condition through computer vision
* analyze the collected data and store the data (frequency and movement cycles)
* Using neural network to train the collected data to achieve the same diagnosis as the doctor.

### Code ###
* **track_blue_red_dot.cpp** can generate trajectory from blue and red stickers

* **Calculation_open_or_close_and_velocity.ipynb** can calculate the property we need for training CNN

* **quant_PD_CNN.ipynb** is the code for training the CNN

* **multiple_device_all_camera.py** can collect data from RGM and two MONO camera with two OAK-D devices at the same time

### Data preprocessing ###
Once we got the raw data from camere we do five step for make the data easiler to train

1. Crop the hand region.

2. Resize the image to 60x50 pixels

3. Make the image from RGB to Gray

4. Used Jackknife to sample the data, each time we sampled 30 frame we did it 300 times for every data 

5. Send the data we sampled to train the CNN

![picture](Figures/Data_preprocessing.png)


### Result ###
* **The mov class best result** 

![picture](Figures/mov_class_best_pred-1.png)

* **The best velocity prediction** 

![picture](Figures/vel_best_pred-1.png)